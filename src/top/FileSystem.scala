package top

trait FileSystem
{

	def children(location: String): Array[String]

	def files(location: String): Array[String]

	def folders(location: String): Array[String]

	def isFile(location: String): Boolean

	def isFolder(location: String): Boolean

	def location(location: String): String

	def parent(location: String): String

}
