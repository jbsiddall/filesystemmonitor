package top

trait Scanner
{
	def scan(location: String): Iterator[String]

}
