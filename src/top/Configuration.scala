package top

import com.google.inject.AbstractModule
import com.google.inject.name.Names

class Configuration extends AbstractModule
{
	def configure()
	{
		bind(classOf[FileSystem]).to(classOf[impl.FileSystem])
		bind(classOf[Hasher]).to(classOf[impl.Hasher])
		bind(classOf[Scanner]).to(classOf[impl.BreadthFirstScanner])
		bind(classOf[SnapShotBuilder]).to(classOf[impl.SnapShotBuilder])

		bindConstant().annotatedWith(Names.named("hash")).to("SHA-512")
		bindConstant().annotatedWith(Names.named("refill")).to(1000)
		bindConstant().annotatedWith(Names.named("outputUpdate")).to(1000)

	}
}
