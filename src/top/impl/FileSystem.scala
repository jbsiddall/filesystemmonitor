package top.impl
import java.io.File

class FileSystem extends top.FileSystem
{


	def children(l: String) = children(l, (f: File) => true)

	private def children(l: String, filter: File => Boolean) = new File(location(l)).listFiles().filter(f => !isSymlink(f)).filter(filter).map(_.getAbsolutePath).toList.toArray

	def files(l: String) = children(l, _.isFile)

	def folders(l: String) = children(l, _.isDirectory)

	def location(l: String) = new File(l).getAbsolutePath

	def parent(l: String) = new File(location(l)).getParent

	def isFile(l: String) = new File(location(l)).isFile

	def isFolder(l: String) = new File(location(l)).isDirectory


	def isSymlink(f: File) = {

		val cp = f.getParentFile.getCanonicalFile
		val f1 = new File(cp, f.getName)

		!f1.equals(f1.getCanonicalFile)

	}

}
