package top.impl

import java.security.MessageDigest
import com.google.inject.Inject
import java.io.{FileInputStream, BufferedInputStream, BufferedReader, FileReader}
import javax.inject.Named

class Hasher @Inject()(@Named("hash") algorithm: String) extends top.Hasher
{
	def hash(location: String) = {
		val digester = MessageDigest.getInstance(algorithm)
		val stream = new BufferedInputStream(new FileInputStream(location))

		var good = true
		while (good)
		{
			val theByte = stream.read()
			if(theByte == -1) good = false
			else digester.update(theByte.toByte)
		}

		val hash = digester.digest()

		stream.close()

		val sum = new StringBuilder()
		val max = Long.MaxValue - Byte.MaxValue - 1
		for(h <- hash)
		{
			sum.append(h)
		}

		sum.toString
	}

}
