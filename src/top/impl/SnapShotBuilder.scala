package top.impl

import com.google.inject.Inject
import collection.mutable.Queue
import top.{SnapShot, FileStatus}
import com.google.inject.name.Named

class SnapShotBuilder @Inject()
(
	scanner: top.Scanner,
	hasher: top.Hasher,
	@Named("outputUpdate") update: Int
) extends top.SnapShotBuilder
{
	def snapShot(location: String): top.SnapShot = {

		val files = scanner.scan(location)

		val workers = 1.to(Runtime.getRuntime.availableProcessors()).map(i => worker(files))

		workers.foreach(_.start())

		var previous = 0
		while(workers.map(_.isAlive).reduce(_ & _))
		{
			if(changed)
			{
				changed = false
				println("files completed %d".format(workers.map(_.done).reduce(_ + _)))

			}
			Thread.sleep(update)

		}

		val finalQueue = workers.map(_.queue).reduce((a,b) => {a.enqueue(b.dequeueAll(f => true):_*); a})

		new SnapShot(location, finalQueue.toArray)
	}

	var changed = false


	def worker(iterator: Iterator[String]) = new Thread() {

		val queue = Queue[FileStatus]()
		var done = 0

		override def run()
		{

			while(iterator.hasNext)
			{
				val file = iterator.next()
				val hash = hasher.hash(file)

				queue.enqueue(new FileStatus(file, System.currentTimeMillis(), hash))
				done += 1
				changed = true
			}

		}
	}
}
