package top.impl

import collection.mutable.Queue
import com.google.inject.Inject
import com.google.inject.name.Named

class BreadthFirstScanner @Inject()
(
	fileSystem: top.FileSystem,
	@Named("refill") refill: Int
) extends top.Scanner
{
	def scan(location: String): Iterator[String] = {

		val isFile = fileSystem.isFile(location)
		val isFolder = fileSystem.isFolder(location)

		new Iterator[String]() {

			private val files = if(isFile) Queue[String](location) else Queue[String]()

			private val folders = if(isFolder) Queue[String](location) else Queue[String]()

			def hasNext = synchronized {
				if(shouldRePopulate()) populateFiles()
				!files.isEmpty
			}

			def next(): String = synchronized {
				if(shouldRePopulate()) populateFiles()

				if(hasNext) files.dequeue()
				else		null

			}

			def shouldRePopulate() = {
				files.isEmpty & !folders.isEmpty
			}

			def populateFiles() = {

				while(files.size < refill & !folders.isEmpty)
				{
					val location = folders.dequeue()
					files.enqueue(fileSystem.files(location):_*)
					folders.enqueue(fileSystem.folders(location):_*)
				}

			}

		}
	}
}
