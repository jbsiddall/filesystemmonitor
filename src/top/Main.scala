package top

import org.yaml.snakeyaml.Yaml
import com.google.inject.{Inject, Guice}
import scala.Predef._
import util.Sorting
import java.io.{FileReader, FileWriter, File}

object Main
{

	val flags = Map(
		"dir" -> "root folder to analyse",
		"file" -> "file to analyse",
		"save" -> "location to save results",
		"diff" -> "location of previous results to diff against"
	)

	def required(flags: Set[String]) = {
		(flags.contains("dir") ^ flags.contains("file")) & (flags.contains("save") ^ flags.contains("diff"))
	}

	def test(condition: => Boolean, syntax: String, msg: String) = {

		val t = () => condition
		if(!t())
		{
			println("%s\nsyntax: %s".format(msg, syntax))
			sys.exit(1)
		}

	}


	def parse(args: Array[String]): Map[String, String] = {

		val flag = "--" + (_:String)
		val deFlag = (_:String).drop(2)

		val syntax = flags.keySet.map(f => flag(f)).map("[" + _ + " <value>]").mkString(" ")

		test(args.size >= 4, syntax, "requires dir or file flag with save or diff flag")

		test(args.size % 2 == 0, syntax, "arguments must be in key value pairs\nsyntax: %s".format(syntax))


		val keyValueArguments = for(i <- 0.until(args.size, 2)) yield args(i) -> args(i+1)

		test(keyValueArguments.map(_._1.startsWith("--")).reduce(_ & _),syntax, "syntax: %s".format(syntax))


		val structured = keyValueArguments.map{case (key, value) => key.drop(2) -> value}.toMap

		test(required(structured.keySet), syntax, "required arguments not given\nsyntax: %s".format(syntax))

		structured

	}

	def main(args: Array[String])
	{
		val structured = parse(args)

		val injector = Guice.createInjector(new Configuration)

		injector.injectMembers(this)

		val snapShot = if(structured.contains("dir")) handleDir(structured("dir")) else handleFile(structured("file"))

		if(structured.contains("save")) handleSave(structured("save"), snapShot)
		else handleDiff(snapShot, structured("diff"))

	}

	@Inject val snapShotBuilder: SnapShotBuilder = null

	val yaml = new Yaml()

	def handleDir(dir: String): SnapShot = {
		snapShotBuilder.snapShot(dir)
	}

	def handleFile(file: String): SnapShot = {
		snapShotBuilder.snapShot(file)
	}


	def handleSave(location: String, snapShot: SnapShot)
	{
		val writer = new FileWriter(location)
		yaml.dump(snapShot, writer)
		writer.close()
		println("saved to %s".format(location))
	}

	def handleDiff(snapShot: SnapShot, location: String)
	{
		val reader = new FileReader(location)
		val old = yaml.load(reader).asInstanceOf[SnapShot]
		reader.close()

		if(snapShot.root != old.root)
		{
			println("can only diff two snapshots with same root.\nold's root is %s, current root is %s".format(old.root, snapShot.root))
			sys.exit(1)
		}

		val oldFiles = old.files.groupBy(_.location)
		val newFiles = snapShot.files.groupBy(_.location)

		val deletedFiles = Sorting.stableSort(oldFiles.keySet.diff(newFiles.keySet).toSeq)
		val createdFiles = Sorting.stableSort(newFiles.keySet.diff(oldFiles.keySet).toSeq)
		val commonFiles = newFiles.keySet.intersect(oldFiles.keySet)

		val changedFiles = Sorting.stableSort(commonFiles.filter(l => oldFiles(l).head.hash != newFiles(l).head.hash).toSeq)

		println("\n\nRESULTS\n")

		if(deletedFiles.isEmpty)
		{
			println("no files have been deleted\n\n")
		}
		else
		{
			println("DELETED FILES:\n%s\n\n".format(deletedFiles.mkString("\n")))
		}

		if(changedFiles.isEmpty)
		{
			println("no files have changed\n\n")
		}
		else
		{
			println("CHANGED FILES:\n%s\n\n".format(changedFiles.mkString("\n")))
		}

		if(createdFiles.isEmpty)
		{
			println("no new files have been created\n\n")
		}
		else
		{
			println("CREATED FILES:\n%s\n\n".format(createdFiles.mkString("\n")))
		}

	}

}
