package top

import reflect.BeanProperty

class FileStatus
{
	def this(location: String, accessed: Long, hash: String) = {
		this()
		this.location = location
		this.accessed = accessed
		this.hash = hash
	}

	@BeanProperty var location: String = null

	@BeanProperty var accessed: Long = -1l

	@BeanProperty var hash: String = null

	override def toString = "FileStatus{location=%s, accessed=%d, hash=%s}".format(location, accessed, hash)

	override def hashCode = toString.hashCode

	override def equals(other: Any) = other match
	{
		case e: FileStatus => e.toString == toString
		case _ => false
	}

}
