package top

import reflect.BeanProperty

class SnapShot
{
	def this(root: String, files: Array[FileStatus]) = {
		this()
		this.root = root
		this.files = files
	}

	@BeanProperty var root: String = null
	@BeanProperty var files: Array[FileStatus] = Array()


	override def equals(other: Any) = other match
	{
		case e: SnapShot => files.toList == e.files.toList & root == e.root
		case _ => false
	}
}
