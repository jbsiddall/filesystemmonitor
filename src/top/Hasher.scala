package top

trait Hasher
{
	def hash(location: String): String
}
